import { IMessage } from './message-type';

export type ChatState = {
    messages: IMessage[];
    preloader: boolean;
    error: string;
    editModal: boolean;
    editedMessage: IMessage | null;
}