import { IMessage } from 'types/message-type';

export interface IMessageService {
    fetchData: <T>(url: string, options: IOptions) => Promise<T>;
    getAllMessages: () => Promise<IMessage[]>;
    addNewMessage: (data: IMessage) => Promise<IMessage>;
    editMessage: (data: IMessage) => Promise<IMessage>;
    deleteMessage: (id: string) => Promise<IMessage>;
}

interface IHeaders {
    contentType: string;
}

interface IOptions {
    method: string;
    headers?: IHeaders;
    body?: string;
}

const { REACT_APP_SERVER_URL } = process.env;
const baseApi = REACT_APP_SERVER_URL as string;

class MessageService implements IMessageService {

    async fetchData<T>(url: string, options: IOptions){
        const res = await fetch(url);
        if(res.status !== 200){
            throw new Error(`Bad request to url: ${url}, with status code ${res.status}` )
        }
        const data: T = await res.json()
        return data
    }

    async getAllMessages(): Promise<IMessage[]>{
        return await this.fetchData<IMessage[]>(baseApi + '/messages', {method: 'GET'});
    }

    async addNewMessage(data: IMessage): Promise<IMessage>{
        const res = await this.fetchData<IMessage>(baseApi + '/messages', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                contentType: 'application/json'
            }
        })
        
        return res
    }

    async editMessage(data: IMessage): Promise<IMessage>{
        const res = await this.fetchData<IMessage>(baseApi + '/messages', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                contentType: 'application/json'
            }
        })
        return res
    }

    async deleteMessage(id: string): Promise<IMessage>{
        const res = await this.fetchData<IMessage>(baseApi + './messages', {
            method: 'DELETE',
            body: JSON.stringify(id),
            headers: {
                contentType: 'application/json'
            }
        })
        return res;
    }
    
}
export default MessageService;