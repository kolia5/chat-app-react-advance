import { IUser } from '../types/user-type';

export interface IUserService {
    fetchData: <T>(url: string) => Promise<T>;
    getAllUsers: () => Promise<IUser[]>
}

const { REACT_APP_SERVER_URL } = process.env;
const baseApi = REACT_APP_SERVER_URL as string;

class UserService implements IUserService {

    async fetchData<T>(url: string){
        const res = await fetch(url);
        if(res.status !== 200){
            throw new Error(`Bad request to url: ${url}, with status code ${res.status}` )
        }
        const data: T = await res.json()
        return data
    }

    async getAllUsers(): Promise<IUser[]>{
        return await this.fetchData<IUser[]>(baseApi + '/users');
    }

    // will change this method and endpoint when i set server correctly
    async login( username: string, password: string): Promise<IUser> {
        const res =  await this.fetchData<IUser[]>(baseApi + '/users')
        return res[0]
    }
    
}
export default UserService;