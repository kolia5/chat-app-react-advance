const dateToHHMM = (date: string): string => {
    const format = new Date(date).toLocaleTimeString('en-gb',{ hour: 'numeric', minute: 'numeric' })
    return format;
}
const dateToFull = (date: string): string => {
    const format = new Date(date).toLocaleTimeString('en-gb',{
        day: '2-digit',
	    month: '2-digit',
	    year: 'numeric',
	    hour: 'numeric',
	    minute: 'numeric'
    })
    return format;
}
const dateToBeautiful = (date: string): string => {
    const nowDate: Date = new Date();
    const yesterdayDate: Date = new Date(new Date().setDate(new Date().getDate() - 1));
    const formatedDate: Date = new Date(date);
    if(nowDate.getFullYear() === formatedDate.getFullYear() &&
        nowDate.getMonth() === formatedDate.getMonth() &&
        nowDate.getDate() === formatedDate.getDate()){
        return 'Today'
    }   
    else if (
        yesterdayDate.getFullYear() === formatedDate.getFullYear() &&
        yesterdayDate.getMonth() === formatedDate.getMonth() &&
        yesterdayDate.getDate() === formatedDate.getDate()
    ){
        return 'Yesterday'
    }   else {
        const format: string = formatedDate.toLocaleDateString('en-gb',{
            weekday: 'long',
            month: 'long',
            day: 'numeric'
        })
        return format;
    }
}

export { dateToHHMM, dateToFull, dateToBeautiful }
