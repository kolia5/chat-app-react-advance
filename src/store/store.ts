import { configureStore } from "@reduxjs/toolkit";

import { chat, users } from './root-reducer';

const store = configureStore({
    reducer: { chat, users }
})

export { store };
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;