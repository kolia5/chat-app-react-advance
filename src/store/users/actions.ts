import { createAsyncThunk } from '@reduxjs/toolkit';
import { IUser } from '../../types/user-type';
import { ActionsType } from './actions-type';
import UserService from '../../services/user-service';

const userService = new UserService();

const getAllUsers = createAsyncThunk<IUser[]>(ActionsType.GET_ALL_USERS, async(_, thunkApi) => {
    const users = await userService.getAllUsers();
    return users;
})

const login = createAsyncThunk<IUser, {username: string, password: string}>(ActionsType.LOGIN,
    async ({ username, password }, thunkApi) => {
        const user = await userService.login(username, password);
        // write user to sessionStorage, will use in my components to check if user been login or role is admin
        if(user) {
            sessionStorage.setItem('userId', user.id);
            sessionStorage.setItem('userRole', user.role);
        }
        return user;
})

export {
    login,
    getAllUsers
}