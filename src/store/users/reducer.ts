import { createReducer } from "@reduxjs/toolkit";
import { UsersState } from "types/users-state-type";
import { getAllUsers, login } from "./actions";

const initialState: UsersState = {
    currentUser: null,
    users: []
}

const reducer = createReducer(initialState, (builder) => {
    builder
        .addCase(login.fulfilled, (state, action) => {
            state.currentUser = action.payload;
        })
        .addCase(getAllUsers.fulfilled, (state, action) => {
            state.users = action.payload;
        })
})

export { reducer };