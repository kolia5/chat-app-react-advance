import { createReducer } from '@reduxjs/toolkit';
import { ChatState } from 'types/chat-state-type';
import { 
    getAllMessages, 
    addNewMessage, 
    editMessage,
    deleteMessage, 
    toggleModal, 
    setEdited, 
    cancelEdited 
} from './actions';

const initialState: ChatState = {
    messages: [],
    preloader: false,
    error: '',
    editModal: false,
    editedMessage: null
}

const reducer = createReducer(initialState, (builder) => {
    builder
        .addCase(getAllMessages.pending, (state, action) => {
            state.error = ''
            state.messages = []
            state.preloader = true;
        })
        .addCase(getAllMessages.fulfilled, (state, action) => {
            state.preloader = false;
            state.messages = action.payload;
        })
        .addCase(getAllMessages.rejected, (state, action) => {
            state.preloader = false;
            state.error = action.error.message as string
        })
        .addCase(addNewMessage.pending, (state, action) => {
            state.preloader = true;
        })
        .addCase(addNewMessage.fulfilled, (state, action) => {
            state.preloader = false;
        })
        .addCase(editMessage.pending, (state, action) => {
            state.preloader = true;
        })
        .addCase(editMessage.fulfilled, (state, action) => {
            state.preloader = false;
        })
        .addCase(deleteMessage.pending, (state, action) => {
            state.preloader = true;
        })
        .addCase(deleteMessage.fulfilled, (state, action) => {
            state.preloader = false;
        })
        .addCase(toggleModal, (state, action) => {
            state.editModal = action.payload;
        })
        .addCase(setEdited, (state, action) => {
            state.editedMessage = action.payload;
        })
        .addCase(cancelEdited, (state, action) => {
            state.editedMessage = action.payload;
        })

            
    
})
export { reducer };