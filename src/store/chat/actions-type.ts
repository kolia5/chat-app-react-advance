const ActionsType = {
    GET_ALL_MESSAGE: 'get-all-message',
    ADD_NEW: 'add-new-message',
    EDIT: 'edit-message',
    DELETE: 'delete-message',
    TOGGLE_MODAL: 'toggle-modal',
    SET_EDITED: 'set-edited',
    CANCEL_EDITED: 'cancel-edited'
}

export { ActionsType };