import LikeIcon from './like-icon';
import DeleteIcon from './delete-icon';
import EditIcon from './edit-icon';
import AttachIcon from './attach-icon';
import LogoIcon from './logo-icon';
import CloseIcon from './close-icon';
export {
    LikeIcon,
    DeleteIcon,
    EditIcon,
    AttachIcon,
    LogoIcon,
    CloseIcon
}