import { useState, useEffect } from 'react';

import { chat as chatActions } from 'store/actions';
import { useAppDispatch } from 'hooks/store-hooks';

import { AttachIcon } from 'components/icons';

import './message-input.scss';

interface MessageInputProps {
    
}

const MessageInput: React.FC<MessageInputProps> = (props) => {
    const [ inputValue, setInputValue ] = useState<string>('');
    const dispatch = useAppDispatch();

    const handleChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        setInputValue(event.target.value); 
    }
    const handleButtonClick: React.MouseEventHandler<HTMLButtonElement> = () => {
        dispatch(chatActions.addNewMessage(inputValue));
        setInputValue('');
    }
    return (
        <div className="message-input">
            <AttachIcon/>
            <input 
                type="text" 
                className="message-input-text" 
                placeholder="Type your message ..."
                value={inputValue}
                onChange={handleChange}
            />
            <button 
                className="message-input-button" 
                type="submit"
                onClick={handleButtonClick}
            >Send</button>
        </div>
    )
}
export default MessageInput;