const ErrorMessage: React.FC = () => {
    return(
        <div className="error-message">
            Something bad happened
        </div>
    )
}
export default ErrorMessage;