import { useState, useEffect, useMemo } from 'react';
import { users as usersActions } from 'store/actions';
import { useNavigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from 'hooks/store-hooks';

import './login.scss';

const Login: React.FC = () => {
    const [ username, setUsername ] = useState<string>('');
    const [ password, setPassword ] = useState<string>('');
    const [ isWarning, setWarning ] = useState<boolean>(false);
    const currentUser = useAppSelector(state => state.users.currentUser);
    const memoUser = useMemo(() => currentUser, []);

    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    
    const loginUser = (loginData: {username: string, password: string}) => {
        dispatch(usersActions.login(loginData));
    }

    const handleOnChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
        const value: string = event.target.value;
        const name: string = event.target.name;
        if(name === 'username')  setUsername(value);
        if(name === 'password') setPassword(value);
    }

    const handleOnSubmit: React.ChangeEventHandler<HTMLFormElement> = (event) => {
        event.preventDefault();
        if(username && password) {
            loginUser({username, password});
            setUsername('');
            setPassword('');
            setWarning(false);
        } else {
            setWarning(true)
        }
    }

    // in this case i am not use localstorage to get userId, i WAIT FOR data from store just for sure
    useEffect(() => {
        currentUser && navigate("/")
    }, [currentUser])

    return  (
        <div className="login">
            <div className="form-container">
                 <h1>Sign in</h1>
                 { isWarning && <p className="login-warning">All fields are required</p> }
                 <form className="login-form" onSubmit={handleOnSubmit}>
                    <input 
                        type="text" name="username" placeholder="username" 
                        value={username} onChange={handleOnChange}/>
                    <input 
                        type="password" name="password" placeholder="password" 
                        value={password} onChange={handleOnChange}/>
                    <p className="forgot-password">Forgot your password?</p>
                    <button type="submit" className="input-button">Login</button>
                 </form>
            </div>
        </div>
    )
}
export default Login;