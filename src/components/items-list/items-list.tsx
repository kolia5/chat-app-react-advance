import Message from 'components/message/message';
import OwnMessage from 'components/own-message/own-message';
import { IMessage } from 'types/message-type';

interface ItemsListProps {
    messages: IMessage[];
}

const ItemsList: React.FC<ItemsListProps> = ({ messages }) => {
    const currentUserId = sessionStorage.getItem('userId')

    const messageItems: JSX.Element[] = messages.map(message => {
        return (
            <li key={message.id}>
                {
                    message.userId !== currentUserId ? <Message item={message}/> : <OwnMessage item={message}/>
                }
            </li>
        )
    })

    return(
        <>
            {messages.length > 0 && messageItems}
        </>
    )
}
export default ItemsList;

