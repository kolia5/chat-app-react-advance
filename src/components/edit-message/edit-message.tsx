import { useAppSelector, useAppDispatch } from 'hooks/store-hooks';
import { IMessage } from 'types/message-type';
import { chat as chatActions } from 'store/actions';

import HeaderToModal from 'components/modal/header-to-modal/header-to-modal';

import './edit-message.scss';
import { useState } from 'react';

interface EditMessageProps {
    onClose: () => void;
}

const EditMessage: React.FC<EditMessageProps> = ({ onClose }) => {
    const editedMessage = useAppSelector(state => state.chat.editedMessage);
    const [ text, setText ] = useState<string | undefined>(editedMessage?.text);

    const dispatch = useAppDispatch();

    const handleChange: React.ChangeEventHandler<HTMLTextAreaElement> = (event) => {
        setText(event.target.value);
    }

    const handleSubmit: React.MouseEventHandler<HTMLButtonElement> = () => {
        const edited = {...editedMessage, text: text} as IMessage
        dispatch(chatActions.editMessage(edited));
        dispatch(chatActions.toggleModal(false));
    }

    return (
        <div className='edit-message'>
            <HeaderToModal title='Edit message' buttonClassName='edit-message-close' onClose={onClose}/>
            <div className='edit-message-body'>
                <textarea 
                    value={text}
                    onChange={handleChange} 
                    className='edit-message-input' cols={30} rows={10}></textarea>
                <button type='submit' onClick={handleSubmit} className='edit-message-button'>Submit</button>
            </div>
        </div>
    )
}
export default EditMessage;