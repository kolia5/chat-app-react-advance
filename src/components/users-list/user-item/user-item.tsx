import { IUser } from 'types/user-type';

import './user-item.scss';

interface UserItemProps {
    user: IUser;
}

const UserItem: React.FC<UserItemProps> = ({ user }) => {
    return (
        <div className='user-item'>
            <div className='username'>{ user.username }</div>
            <button className='edit-btn'>Edit</button>
            <button className='delete-btn'>Delete</button>
        </div>
    )
}
export default UserItem;