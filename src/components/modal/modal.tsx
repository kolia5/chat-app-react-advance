import { DOMElement, useEffect } from 'react';

import './modal.scss';

interface ModalProps {
    children: React.ReactNode;
    isOpen: boolean;
    onClose: () => void;
}

const Modal: React.FC<ModalProps> = ({ children, isOpen, onClose }) => {

    const handleOverlayCheck: React.MouseEventHandler<HTMLElement> = (event) => {
        if((event.target as HTMLElement).classList.contains('modal-shown')) {
            onClose();
        } 
    };
    
    useEffect(() => {
        document.documentElement.style.overflowY = 'hidden';
        document.querySelector('.edit-message-modal')?.classList.add('modal-shown');
        
    
        return () => {
          document.documentElement.style.overflowY = '';
          document.querySelector('.edit-message-modal')?.classList.remove('modal-shown');
        };
    });
    return (
        <>
            {isOpen && (
                <div className='edit-message-modal' onClick={handleOverlayCheck}>
                    {children}
                </div>
            )}
        </>
    );
}
export default Modal;

