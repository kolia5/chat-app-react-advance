import { IMessage } from '../../../types/message-type';

const getTimeSpans = (mess: IMessage[]): string[] => {

    const dateSpans: string[] = [];
    mess.forEach(item => {
        // editedAt can be empty, so check it and if empty use createdAt
        if(item.editedAt){
            dateSpans.push(item.editedAt)
        } else {
            dateSpans.push(item.createdAt)
        }
    })
    const sortedDateSpans = dateSpans.sort((a, b) => new Date(a).getTime() - new Date(b).getTime());
    // delete dublicate
    const dateDivider = new Set(sortedDateSpans); 
    const arr = Array.from(dateDivider);
    return arr
}
const filterByDate = (items: IMessage[], date: string): IMessage[] => {
    const filtered = items.filter(item => {
        // editedAt can be empty, so check it and if empty use createdAt
        if(item.editedAt){
            return item.editedAt === date
        } else {
            return item.createdAt === date
        }
    })
    return filtered
}

export {
    getTimeSpans, filterByDate
}