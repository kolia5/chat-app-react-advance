import {IMessage } from 'types/message-type';

import ItemsList from 'components/items-list/items-list';
import { getTimeSpans, filterByDate } from './helpers/helpers';
import { dateToBeautiful } from 'helpers/date-formating';

import './message-list.scss';

interface MessageListProps {
    messages: IMessage [];
}

const MessageList: React.FC<MessageListProps> = ({ messages }) => {
    // get time dividers from all messages
    const dividers = getTimeSpans(messages);
    
    const sections: JSX.Element[] = dividers.map(date => {
        return (
            <div key={date}>
                <ul className="messages-divider">
                    <ItemsList 
                        messages={filterByDate(messages, date)}
                    />
                </ul>
                <div className="date-label">
                    {dateToBeautiful(date)}
                </div>
            </div>
        )
    })
    
    return (
        <div className="message-list">
            {sections}
        </div>
    )
}
export default MessageList;