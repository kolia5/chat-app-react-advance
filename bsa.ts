import Chat from './src/components/chat/chat';
import { chat, users } from './src/store/root-reducer';

export default {
    Chat,
    rootReducer: {chat, users}
};